# Ansible test roles (with molecule tests) #

It's a project to create some common roles for base configuration
and their tests.

## Start a new role ##

    molecule init role -r <role-name> -d <platform>

## Links ##

- [How To Test Ansible Roles with Molecule](https://www.digitalocean.com/community/tutorials/how-to-test-ansible-roles-with-molecule)
- [testinfra Modules](https://testinfra.readthedocs.io/en/stable/modules.html)
