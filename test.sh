#!/bin/bash

#vagrant destroy -f
vagrant up && \
    ansible-playbook \
    -i inventory/test \
    -u vagrant \
    --private-key=~/.ssh/id_rsa \
    --limit 'test-machines' \
    $1 && \
    notify-send --urgency=low "hey you!" "take a look..." && \
    vagrant ssh || \
    notify-send --urgency=low "hey you!" "Oops..."
