import os

import testinfra.utils.ansible_runner

import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


@pytest.mark.parametrize("name,version", [
    ("python-mysqldb", "1.3"),
    ("mysql-server", "5.7"),
])
def test_packages(host, name, version):
    assert host.package(name).is_installed
    assert host.package(name).version.startswith(version)


@pytest.mark.parametrize('svc', [
  'mysql',
])
def test_svc(host, svc):
    service = host.service(svc)

    assert service.is_running
    assert service.is_enabled


def mysql_created_users(host):
    cmd = host.run("mysql -u app -MyDatabasePassword --execute=\"SELECT 1\"")
    assert cmd.stderr == ''
