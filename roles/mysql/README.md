Role mysql
=========

A role to install and configure MySQL.

Requirements
------------

For now it works only in Ubuntu servers.

Role Variables
--------------

The only variable is to set the MySQL root password (`mysql_root_password`).

Dependencies
------------

No dependencies.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: mysql, mysql_root_password: "My Strong Password" }

License
-------

BSD

Author Information
------------------

Eldius
https://github.com/Eldius
https://bitbucket.org/Eldius/
